include p4_file.asm 
include p4_color.asm



;#-----------------------------------------------#
;#                 Krlos López                   #
;#                  201313894                    #
;#             ♡ Debian + Gnome ♡                #
;#-----------------------------------------------#

   

; ***
; IMPRIMIR CADENAS Y EL VECTOR PARA FORMAR EL TABLERO
; ***
imprimir macro cadena 
  mov ax,@data  ; carga el segmento de datos para obtener los datos
  mov ds,ax     ; en el segmento de datos
  mov ah,09h    ; 
  mov dx,offset cadena  ; Direccion del texto a imprimir
  int 21h       ; interrupcion del DOS 21h 
endm


; ***
; CAPTURA LA OPCION TOMADA DE LA TERMINAL 
; ***
capturarCaracter macro
  mov ah, 01h ;mueve el primer caracter de entrada a la posicion del registro 'ah'
  int 21h
endm





pedirComandoEnConsola macro 
  mov dx,offset msj_ingresoComando  ;Imprimimos el msj_ingreseComando
  int 21h
  lea si,vec_comandoEntrada  ;Cargamos en el registro si al primer vector

  pedir1:;tomo el comando a ingresar
    mov ah,01h  ;Pedimos un carácter
    int 21h
    mov [si],al  ;Se guarda en el registro indexado al vector
    inc si
    cmp al,0dh  ;Se cicla hasta que se digite un Enter
    ja pedir1
    jb pedir1
endm




.model small
.stack
.data
  ;**************************************************************************************************************
  ;*******************************************  ENCABEZADO  *****************************************************
  ;**************************************************************************************************************
  encabezado1 db 0ah, 0dh, "   * UNIVERSIDAD DE SAN CARLOS DE GUATEMALA            ", "$"
  encabezado2 db 0ah, 0dh, "   * ARQUITECTURA DE COMPUTADORES Y ENSAMBLADORES 1    ", "$"
  encabezado3 db 0ah, 0dh, "   * Carlos Arnoldo Lopez Coroy - 201313894            ", "$"
  encabezado4 db 0ah, 0dh, "   * Practica 4                                        ", "$"
  encabezado5 db 0ah, 0dh, "   *                                                   ", "$"
  encabezado6 db 0ah, 0dh, "   * Ingrese x si desea cerrar el programa             ", "$"
  salto db 0ah, 0dh, "$"




  ; MENSAJES VARIOS  **********************************************
  msj_colorBienvenida  db 0ah,0dh,'de ahuvo entraste a colorear ','$'
  saludo db 0Ah,0Dh, "Anlaizando texto..........","$"

  esPalabraDiptongo db 0ah, 0dh, 'Es un diptongo','$'
  noEsPalabraDiptongo db 0ah, 0dh, 'No es un diptongo','$'

  esPalabraHiato  db 0ah, 0dh, 'Es un Hiato','$'
  noEsPalabraHiato  db 0ah, 0dh, 'No es un Hiato','$'

  esPalabraTrip  db 0ah, 0dh, 'Es un Triptongo','$'
  noEsPalabraTrip  db 0ah, 0dh, 'No es un Triptongo','$'

  ; ERRORES *******************************************************
  error_archivoAbrir db 0ah,0dh,'Error al abrir el archivo','$'
  error_archivoLeer db 0ah,0dh,'Error al leer archivo','$'
  error_comandoEntrada db 0ah,0dh, 'Comando no existe ', '$'


  ; VARIABLES PARA EL ARCHIVO *************************************
  rutaArchivo db 400 dup('$'), '$'   ;capturo la ruta
  vectorReporte db 400 dup('$'), '$' ;bufer para generar el reporte.
  handleFichero dw ?
  bufferContenidoArchivo db 400 dup('$'), '$'   ;contenido del archivo
  bufferDiptongo db 100 dup('$'),'$'            ;-diptongo_"palabra_a_analizar"
  bufferHiato db 100 dup('$'),'$'               ;-hiato_"palabra_a_analizar"
  bufferTriptongo db 100 dup('$'),'$'           ;-triptongo_"palabra_a_analizar"


  ; VARIABLES UTILIZADAS PARA ENTRADAS*****************************
  msj_ingresoComando db 0ah,0dh, 'Ingrese el comando:  ', '$'
  vec_comandoEntrada db 50 dup('$'), '$' 


  ; VECTORES PARA COMPARAR LOS COMANDOS DE ENTRADA ****************
  default_comando0 db 'x','$'
  default_comando1 db '-abrir_','$'
  default_comando2 db '-colorear','$'
  default_comando3 db '-diptongo_','$'
  default_comando4 db '-hiato_','$'
  default_comando5 db '-triptongo_','$'
  default_comando6 db '-reporte','$'
  default_comando7 db '-contar_diptongo','$'


  ; VALORES PARA MODO VIDEO****************************************
  fila db 0
  columna db 0
  contDip db 0



  Flag db 0











.code
main proc

  mov ax,@data
  mov ds,ax
  mov ah,09

  inicio:
      imprimir encabezado1
      imprimir encabezado2
      imprimir encabezado3
      imprimir encabezado4
      imprimir encabezado5
      imprimir encabezado6
      imprimir salto
      imprimir salto
      pedirComandoEnConsola
      jmp comandos

  ;=========================================================================================================================
  ;===========================================  RECONOCER LOS COMANDO DE EJECUCION  ========================================
  ;=========================================================================================================================
  comandos: 
      mov cx,7 ; -abrir_archivo
      mov ax,ds
      mov es,ax 
      lea si, vec_comandoEntrada
      lea di, default_comando1 
      repe cmpsb
        je abrirArchivoPalabras


      mov cx,8 ; -colorear
      mov ax,ds
      mov es,ax 
      lea si, vec_comandoEntrada
      lea di, default_comando2 
      repe cmpsb
        je colorearPalabras

      mov cx,10  ;-diptongo_
      mov ax,ds
      mov es,ax
      lea si, vec_comandoEntrada
      lea di, default_comando3
      repe cmpsb 
        je identificarPalabraDiptongo


      mov cx,7  ;-hiato_
      mov ax,ds
      mov es,ax
      lea si, vec_comandoEntrada
      lea di, default_comando4
      repe cmpsb 
        je identificarPalabraHiato

      mov cx,11  ;-triptongo_
      mov ax,ds
      mov es,ax
      lea si, vec_comandoEntrada
      lea di, default_comando5
      repe cmpsb 
        je identificarPalabraTriptongo

      
      mov cx,7 ; -reporte
      mov ax,ds
      mov es,ax 
      lea si, vec_comandoEntrada
      lea di, default_comando6
      repe cmpsb 
        je generarReporte

      mov cx,15 ; -contar_diptongo
      mov ax,ds
      mov es,ax 
      lea si, vec_comandoEntrada
      lea di, default_comando7
      repe cmpsb 
        je contarTriptongoPalabra


      mov cx,1 ; salir
      mov ax,ds
      mov es,ax 
      lea si, vec_comandoEntrada
      lea di, default_comando0
      repe cmpsb 
        je salir






  ;==========================================================================================================
  ;==========================================================================================================
  ;==========================================================================================================
  ;==========================================================================================================
  abrirArchivoPalabras:
      pathArchivo rutaArchivo, vec_comandoEntrada, 7h ;  que lea de la posicion #7 del vector donde ya se indica la ruta a utilizar
      ;imprimir rutaArchivo ; corroboro que imprima
      abrirArchivo rutaArchivo, handleFichero
      ;Seccion para leer el archivo
      leerArchivo SIZEOF bufferContenidoArchivo,bufferContenidoArchivo, handleFichero
      imprimir salto
      imprimir bufferContenidoArchivo
      capturarCaracter
      jmp inicio


  colorearPalabras:
      je activarModoVideo
      capturarCaracter
      jmp inicio


  identificarPalabraDiptongo:
      pathArchivo bufferDiptongo, vec_comandoEntrada, 10
      je palabraDiptongo
      jmp inicio
      
  identificarPalabraHiato:
      pathArchivo bufferHiato, vec_comandoEntrada, 7h
      imprimir bufferHiato
      je palabraHiato
      jmp inicio
    
  identificarPalabraTriptongo:
      pathArchivo bufferTriptongo, vec_comandoEntrada, 11
      imprimir bufferTriptongo
      je palabraTriptongo
      jmp inicio



  contarTriptongoPalabra:
      je contarDiptongo
      jmp inicio
  
  generarReporte:
      ;je palabraReporte
      ;imprimir vectorReporte
      ;jmp inicio





;;************************************************************************************************************************************
;;*****  MODO ACTIVAR COLOR  *********************************************************************************************************
;;************************************************************************************************************************************
  activarModoVideo:
      imprimir msj_colorBienvenida
      ; se inicializa modo video con una resolucion de 80x25
      mov ah, 0
      mov al, 03h
      int 10h

      imprimir saludo
      imprimir salto

      mov ah, 03h
      mov bh, 00h
      int 10h ;dh guarda el valor de la ultima posicion fila y dl guarda la ultima posicion de la columna

      mov fila, dh
      mov columna, dl
      mov si, 0
      mov di, 0
    
  ciclo1:
      mov al, 0
      ;posicionar al cursor donde corresponde
      posicionarCursor fila, columna
      
      esTriptongo bufferContenidoArchivo[si], bufferContenidoArchivo[si+1], bufferContenidoArchivo[si+2]
      cmp al, 3h ; indica que es triptongo
        je coloreoTriptongo

      esDiptongo bufferContenidoArchivo[si], bufferContenidoArchivo[si+1]
      cmp al, 1h ; indica que es diptongo
        je coloreoDiptongos

      esHiato bufferContenidoArchivo[si], bufferContenidoArchivo[si+1]
      cmp al, 2h ; indica que es hiato
        je coloreoHiatos

      cmp al,0h  
        je letra

  coloreoDiptongos:
      imprimirVideo bufferContenidoArchivo[si], 0010b ;imprimos VERDE
      inc columna ;aumenta la posicion del cursor
      inc si

      posicionarCursor fila, columna
      imprimirVideo bufferContenidoArchivo[si], 0010b ;imprimos VERDE
      jmp siguiente


  coloreoHiatos:
      imprimirVideo bufferContenidoArchivo[si], 0100b ;imprimos ROJO
      inc columna ;aumenta la posicion del cursor
      inc si

      posicionarCursor fila, columna
      imprimirVideo bufferContenidoArchivo[si], 0100b ;imprimos ROJO
      jmp siguiente


  coloreoTriptongo:
      imprimirVideo bufferContenidoArchivo[si], 1110b ;imprimos AMARILLO
      inc columna ;aumenta la posicion del cursor
      inc si

      imprimirVideo bufferContenidoArchivo[si], 1110b ;imprimos AMARILLO
      inc columna ;aumenta la posicion del cursor
      inc si

      posicionarCursor fila, columna
      imprimirVideo bufferContenidoArchivo[si], 1110b ;imprimos AMARILLO
      jmp siguiente



  letra:
      imprimirVideo bufferContenidoArchivo[si], 1111b ;imprimos blanco
      jmp siguiente



  siguiente:
      inc columna ;aumenta la posicion del cursor
      inc si

      cmp columna, 80d
      jl noSalto
        mov columna,0
        inc fila
  noSalto:
      cmp bufferContenidoArchivo[si], 36d 
      jne ciclo1

      inc fila
      mov ah,02h
      mov dh,fila
      mov dl,0
      mov bh,0
      int 10h
      je salir

  jne inicio
;;************************************************************************************************************************************
;;************************************************************************************************************************************
;;************************************************************************************************************************************




;;************************************************************************************************************************************
;;*****  ANALIZA PALABRA DIPTONGO  ***************************************************************************************************
;;************************************************************************************************************************************
  palabraDiptongo:
      mov si, 0
      mov di, 0

  cicloDip:      
      esDiptongo bufferDiptongo[si], bufferDiptongo[si+1]
      
      cmp al, 1h ; indica que es diptongo
        je esDip
      
      inc si
      cmp bufferDiptongo[si], 36d
      jne cicloDip
      je noesDip

    esDip:
      imprimir esPalabraDiptongo
      imprimir salto 
      jmp inicio

    noesDip:
      imprimir noEsPalabraDiptongo
      imprimir salto
      jmp inicio

  jne inicio
;;************************************************************************************************************************************
;;************************************************************************************************************************************
;;************************************************************************************************************************************



;;************************************************************************************************************************************
;;*****  ANALIZA PALABRA HIATO  ******************************************************************************************************
;;************************************************************************************************************************************
  palabraHiato:
      mov si, 0
      mov di, 0

  cicloHia:
      esHiato bufferHiato[si], bufferHiato[si+1]
      
      cmp al, 2h ; indica que es diptongo
        je esHia
      
      inc si
      cmp bufferHiato[si], 36d
      jne cicloHia
      je noesHia

    esHia:
      imprimir esPalabraHiato
      imprimir salto 
      jmp inicio

    noesHia:
      imprimir noEsPalabraHiato
      imprimir salto
      jmp inicio

  jne inicio
;;************************************************************************************************************************************
;;************************************************************************************************************************************
;;************************************************************************************************************************************





;;************************************************************************************************************************************
;;*****  ANALIZA PALABRA TRIPTONGO  **************************************************************************************************
;;************************************************************************************************************************************
  palabraTriptongo:
      mov si, 0
      mov di, 0

  cicloTri:
      esTriptongo bufferTriptongo[si], bufferTriptongo[si+1], bufferTriptongo[si+2]
      
      cmp al, 3h ; indica que es diptongo
        je esTri
      
      inc si
      cmp bufferTriptongo[si], 36d
      jne cicloTri
      je noesTri

    esTri:
      imprimir esPalabraTrip
      imprimir salto 
      jmp inicio

    noesTri:
      imprimir noEsPalabraTrip
      imprimir salto
      jmp inicio

  jne inicio
;;************************************************************************************************************************************
;;************************************************************************************************************************************
;;************************************************************************************************************************************




;;************************************************************************************************************************************
;;*****  CONTEO PALABRA DIPTONGO  ***************************************************************************************************
;;************************************************************************************************************************************
  contarDiptongo:
      mov si, 0
      mov di, 0
      
      

  cicloDipCont:      
      esDiptongo bufferContenidoArchivo[si], bufferContenidoArchivo[si+1]
      
      cmp al, 1h ; indica que es diptongo
        inc di
      
      inc si
      cmp bufferContenidoArchivo[si], 36d
      jne cicloDipCont
      ;mov dl, di
      ;mov ah, 02h
      int 21h

  jne inicio
;;************************************************************************************************************************************
;;************************************************************************************************************************************
;;************************************************************************************************************************************





















  ;***************************************************************************************
  ErrorAbrir:
      ;imprimir error_archivoAbrir
      jmp inicio
  ErrorLeer:
      imprimir error_archivoLeer
      capturarCaracter
      jmp inicio
  salir:
      mov ah, 4ch
      xor al, al
      int 21h
  ;***************************************************************************************



 


main endp
end main
