;;
escribirChar macro char
		mov ah, 02h
		mov dl, char
		int 21h
endm
   
posicionarCursor macro x,y
	mov ah,02h;devuelve el caracter del registro
	mov dh,x
	mov dl,y;lo envia 
	mov bh,0
	int 10h
endm

imprimirVideo macro caracter, color
	mov ah, 09h;visualiza un caracter
	mov al, caracter ;al guarda el valor que vamos a escribir
	mov bh, 0
	mov bl, color ; valor binario rojo
	mov cx,1
	int 10h
endm






esDiptongo macro caracter1, caracter2 ;en el registor al va a tener un 1 si es un diptongo o un 0 si no lo es
	LOCAL salida, esDipto, esA, esE, esIi, esO, esU, esDip
	mov al,0

    ;=========dipto=========
    esA:
        cmp caracter1,97    ; a
            jne esE
        cmp caracter2,105   ; i
            je esDip
        cmp caracter2, 117  ; u
            je esDip
        jmp salida

    esE:
        cmp caracter1,101   ; e
            jne esIi
        cmp caracter2,105   ; i 
            je esDip
        cmp caracter2, 117  ; u
            je esDip
        jmp salida
    esIi:
        cmp caracter1,105   ;i
            jne esO
        cmp caracter2,97    ;a
            jne esDip
        cmp caracter2,101   ;e
            je esDip
        cmp caracter2,111   ;o
            jne esDip
        cmp caracter2,117   ;u
            je esDip
        jmp salida
    esO:
        cmp caracter1,111   ; o
            jne esU
        cmp caracter2,105   ; i
            je esDip
        cmp caracter2, 117  ; u
            je esDip
        jmp salida
    
    esU:
        cmp caracter1,117   ;u
            jne salida
        cmp caracter2,97    ;a
            je esDip
        cmp caracter2,101   ;e
            je esDip
        cmp caracter2,111   ;o
            je esDip
        jmp salida


    esDip:
        mov al,1h
	    jmp salida
    
	salida:
endm


esHiato macro caracter1, caracter2 ;en el registor al va a tener un 1 si es un diptongo o un 0 si no lo es
LOCAL salida, esA, esE, esIi, esO, esU, esHia
	mov al,0

    ;=========HIATO=========
    esA:
        cmp caracter1,97    ; a
            jne esE
        cmp caracter2,97    ; a
            je esHia
        cmp caracter2,101   ; e
            je esHia
        cmp caracter2,111   ; o
            je esHia
        jmp salida
    esE:
        cmp caracter1,101   ;e
            jne esIi 
        cmp caracter2,101   ;e
            je esHia
        cmp caracter2,97    ;a
            je esHia
        cmp caracter2,111   ;o
            je esHia
        jmp salida
    esIi:
        cmp caracter1,105   ;i
            jne esO 
        cmp caracter2,105   ;i
            je esHia
        jmp salida
    esO:
        cmp caracter1,111   ;o
            jne esU 
        cmp caracter2,111   ;o
            je esHia
        cmp caracter2,97    ;a
            je esHia
        cmp caracter2,101   ;e
            je esHia
        jmp salida
    esU:
        cmp caracter1,117   ;u
            jne salida
        cmp caracter2,117   ;u
            je esHia
        jmp salida


    esHia:
        mov al,2h
	    jmp salida
    
	salida:
endm



esTriptongo macro caracter1, caracter2, caracter3 ;en el registor al va a tener un 1 si es un diptongo o un 0 si no lo es
LOCAL salida, esIU, esA, esUA, esUE, esE, esIi, esO, esU, esTrip
	mov al,0

    ;=========TRIPTONGO=========
    esIU:
        cmp caracter1,105   ;i
            jne esA
        cmp caracter1,117   ;u
            jne esUA
        jmp salida
    esA:
        cmp caracter2,97    ;a
            jne esE
        cmp caracter3,105   ;i
            je esTrip
        cmp caracter3,117   ;u
            je esTrip
        jmp salida
    esE:
        cmp caracter2,101   ;e
            jne esO
        cmp caracter3,105   ;i
            je esTrip
        cmp caracter3,117   ;u
            je esTrip
        jmp salida
    esO:
        cmp caracter2,111   ;o
            jne salida
        cmp caracter3,105   ;i
            je esTrip
        jmp salida
    esUA:
        cmp caracter2,97    ;a
            jne esUE 
        cmp caracter3,105   ;i
            je esTrip
        cmp caracter3,117   ;u
            je esTrip
        jmp salida
    esUE:
        cmp caracter2,101   ;e
            jne salida 
        cmp caracter3,105   ;i
            je esTrip
        jmp salida

    esTrip:
        mov al,3h
	    jmp salida
    
	salida:
endm