 
;**************************************************************************************************************
;************************************  SECCION PARA ABRIR EL ARCHIVO  *****************************************
;**************************************************************************************************************
pathArchivo macro rutaArchivo, comandoE, posicion
  LOCAL INICIO,FIN
    xor si,si
    xor di, di 
    mov si, posicion
   
  INICIO:
    mov al, comandoE[si]
    cmp al, 0dh;24h
    je FIN 
    mov rutaArchivo[di], al
    inc si
    inc di 
    jmp INICIO

  FIN:
    mov rutaArchivo[si], 00h ; colocar como ultimo caracter $ a la cadena
endm

abrirArchivo macro ruta, handle
  mov ah,3dh
  mov al,02h;010b
  lea dx,ruta
  int 21h     ;ejecuto para que vaya a leer el archivo
  mov handle,ax
    jc ErrorAbrir
endm

leerArchivo macro numbytes, buffer, handle
  mov ah,3fh ;lectura para el fichero
  mov bx,handle ;
  mov cx,numbytes
  lea dx,buffer;desplazamiento de caracteres leidos
  int 21h
  jc ErrorLeer
endm

limpiarVector macro buffer, numbytes, caracter
LOCAL Repetir
push si
push cx

	xor si,si
	xor cx,cx
	mov	cx,numbytes
	;si = 0
	Repetir:
		mov buffer[si], caracter
		inc si ; si ++
		Loop Repetir
pop cx
pop si
endm
;*************************************************************************************************************** FIN




